#!/usr/local/bin/python3
# nodemon --exec python3 process.py
import os
import subprocess
from subprocess import PIPE, run
import cv2
import os
import platform
import exifread
import datetime
import re
from datetime import datetime, timedelta
from vendor import database
from vendor import read_write_model
import numpy as np

output_dir = "OUTPUT"
input_csv = "/Users/andrew/Desktop/RoomScan/smaller.csv"
input_file = "/Users/andrew/Desktop/RoomScan/IMG_4299.MOV"
DATE_FORMAT = "%Y:%m:%d %H:%M:%S"
acceptable_range = 30
db_path = output_dir+"/database.db"


# Load data
print("\nParsing location data...")
print("--------------------------")
print("Loading tracker data (may take a while)...")
data = {}
with open(input_csv) as f:
    for index, line in enumerate(f):
        try:
            line_arr = line.split(",")
            dt = line_arr[0]
            line_arr.pop(0)
            epoch_ts = datetime.strptime(
                dt.split(".")[0], "%Y-%m-%d %H:%M:%S").timestamp()
            pose = ' '.join(map(str, line_arr)).strip().split(" ")
            data[epoch_ts] = pose
        except:
            pass
print(f"{len(data)} datapoints loaded from '{input_csv}'")


def closest_data_to(timestamp, data):
    distance = 999999
    shortest_distance = distance
    for datatime in list(data.keys()):
        item_distance = abs(timestamp - datatime)
        if item_distance < distance:
            shortest_distance = item_distance
            closest = data[datatime]
    return closest if shortest_distance <= acceptable_range else None


def match_data(timestamp):
    if timestamp in list(data.keys()):
        return data[timestamp]
    else:
        return closest_data_to(timestamp, data)


print("\nGetting video info....")
print("--------------------------")

# Get FPS
video = cv2.VideoCapture(input_file)
fps = video.get(cv2.CAP_PROP_FPS)
width = video.get(cv2.CAP_PROP_FRAME_WIDTH)
height = video.get(cv2.CAP_PROP_FRAME_HEIGHT)
video.release()

# Create date
cmd = "exiftool '%s'" % input_file
output = subprocess.check_output(cmd, shell=True)
for l in output.decode("ascii").split("\n"):
    if "Create Date" in l:
        datetime_str = l.split(" : ")[1]
        create_date = datetime_str
create_date_dt = datetime.strptime(create_date, DATE_FORMAT)
create_date_dt = create_date_dt + timedelta(hours=2)
print(f"Date Created: {create_date}")
print(f"FPS: {int(fps)} | {int(width)}x{int(height)}")


# Generate screenshots with ffmpeg
print("\nGenerating Screenshots....")
print("--------------------------")
if not os.path.exists(output_dir):
    os.mkdir(output_dir)
    run(['ffmpeg',
        '-skip_frame', 'nokey', '-i',
         input_file,
         '-vsync', '0', '-frame_pts', 'true',
         'OUTPUT/pts_%d.jpg'],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.DEVNULL)
else:
    print("SKIPPING GENERATION (already generated)")
frames = next(os.walk(output_dir))[2]
frame_count = len(frames)
print(f"{frame_count} frames")

# Match frames to data
print("\nMatching data...")
print("--------------------------")
discarded_count = 0
images = {}
for entry in sorted(os.scandir(output_dir), key=lambda e: e.name):
    if (entry.path.endswith(".bmp") or entry.path.endswith(".jpg")) and entry.is_file():
        numbers = []
        file_name = entry.path.split("/")[1]
        seconds_offset = float(re.findall(r'\d+', file_name)[0])/fps
        timestamp = (create_date_dt + timedelta(0, seconds_offset)).timestamp()
        position_at_time = match_data(timestamp)
        if not position_at_time:
            discarded_count = discarded_count+1
        else:
            images[file_name] = position_at_time
print(str(discarded_count)+" frames unmatched (" +
      str(int(((frame_count-discarded_count)/frame_count)*100))+"% coverage)")


# Coverts csv data to multidimensional array
def parse_matrix_data(data):
    try:
        d = np.array(data)
        pose_arr = np.zeros((4, 4))
        pose_arr[0] = [d[0], d[1], d[2], d[3]]
        pose_arr[1] = [d[4], d[5], d[6], d[7]]
        pose_arr[2] = [d[8], d[9], d[10], d[11]]
        pose_arr[3] = [d[12], d[13], d[14], d[15]]
        return pose_arr
    except:
        return []


# Create Database for COLMAP
print("\nBuilding database...")
print("--------------------------")
print(f"{len(images.keys())} datapoints")
init_params = np.array((1.5, width/2, height/2))
if os.path.exists(db_path):
    os.remove(db_path)
db = database.COLMAPDatabase.connect(db_path)
db.create_tables()
camera_model = read_write_model.CAMERA_MODEL_NAMES['SIMPLE_PINHOLE']
cam_id = db.add_camera(camera_model.model_id, width, height, init_params)
for image in images:
    transformation_matrix = parse_matrix_data(images[image])
    image_obj = read_write_model.Image(
        camera_id=cam_id, name=image, transformation_matrix=transformation_matrix)
    db.add_image(image=image_obj)
db.commit()
db.close()

print("\nDONE: Ready for colmap!")
