# COLMAP Utilities

Python utilities to make working with [COLMAP](https://colmap.github.io/index.html) from various sources a bit easier.

Note this repo is not a real project, I'm not much of a Python programmer, and there are probably better tools these days (maybe? let me know!), but if it's useful to you that's awesome.

[Based on work done by Alexander Clarke](https://github.com/alexander-clarke/openvr-room-mapping) using openVR headsets to record a space. [I also forked that and organized it to my taste](https://github.com/feralresearch/openvr-room-mapping), but you should probably just go use the original. [Here's a video about it](https://www.youtube.com/watch?v=hUZ_PV7Sbuw).

## Theory of Operation

COLMAP can take a pile of images and reconstruct a 3D model out of it. It needs no additional information to do this and that's pretty damn cool. However, it's understandably slow. COLMAP is actually a pipeline of different operations which builds a database along the way. We can speed up this process a lot by providing "hints" about the scene in this database format which will allow the process to go a lot faster. AC script above does this by using a VR headset to record both location data and images at the same time. This is pretty cool but I didn't like the quality of the images I could get out of my headset and I was curious to see if I could use my webcam or cellphone and just link that to tracker data.

## Hardware

You can use virtually any camera and probably any tracker if you can get the data into the correct format. For this I used HTC Vive trackers and my iPhone 7 and a Logitech webcam. It's not necessary (you can just hold the hardware together while shooting) but I also used this [Cullmann multiclamp](https://www.techinn.com/en/cullman-multi-clamp-cc-50-41150/137842923/p) left over from Kinect hacking work to physically link the tracker and the camera while shooting. I'm running this on a decently powered but not too crazy gaming rig with a 2080ti video card which lets me run COLMAP CUDA. Reconstruction still takes 10min or more (much more if there are a lot of images).

## Software

### vendor

This folder contains code which is not mine (from the above-linked AC project, also [Triad OpenVR wrapper](https://github.com/TriadSemi/triad_openvr) (which makes working with the hardware a little easier) and [COLMAP](https://colmap.github.io/index.html) itself)

### config.json

This is primarily the [configuration file for Triad](https://github.com/TriadSemi/triad_openvr), but I have added a `settings` key, under which you can find settings for the following scripts.

### webcam_recorder.py

[THIS CODE IS CURRENTLY OVER HERE](https://github.com/feralresearch/openvr-room-mapping) (sorry said this was a mess ;) I'll be moving it here eventually)

### puck_recorder.py

This utility expects your computer to have a VIVE tracker setup. Specify which tracker you want in `config.json` and run the script, which will then record locations every X seconds to `output.csv`

### parse_recorded_data.py

This script is intended to parse the `output.csv` from the previous step along with a video file shot at the same time. It uses the timestamp on the video file to synchronize the data, producing a sqlite database and folder of images suitable to feed to COLMAP.

## Notes

- I am curious about trying to reconstruct a pair of stereoscopic images (like from an old-school stereoscope), but COLMAP is not the tool for this. I am fairly sure this can be done using plain OpenCV. Would be neat to use the rust version and WebASM and make a web tool out of it.

# To fix old data (yeah this is hideous)

```sh
#!/bin/bash
tr '\r\n' ',' < $1 > output.csv
sed -i '' -E "s/\]|\[//g" output.csv
sed -i '' -E "s/ /,/g" output.csv
sed -i '' -E 's/,,*/,/g' output.csv
sed -i '' -E 's/,0.,0.,0.,1./,0.,0.,0.,1.\n/g' output.csv
sed -i '' -E 's/,2021-08-28,/2021-08-28 /g' output.csv
sed -i '' -e '$ d' output.csv
rm $1
mv output.csv $1
```
